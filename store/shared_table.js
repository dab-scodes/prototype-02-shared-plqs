import Web3 from 'web3'
import SHARED_TABLE from '~/services/constants/shared_table'
import { ETHERSCAN_URL } from '~/services/ethereum';
import { IPFS_MANAGER } from '~/services/ipfs';
import { Notifier } from '~/services/notifier.js';

export const state = () => ({
  contract: {
    instance: null,
    network: null,
    address: null,
    url: null,
  },
  creator: {
    address: null,
    url: null
  },
  updator: {
    address: null,
    url: null
  },
  hash: null,
  data: {
    remote: null,
    local: null
  },
  read: false
});

export const getters = {
  getContract: state => state.contract,
  getLocalData: state => state.data.local,
  getRead: state => state.read,
  getHash: state => state.hash,
};

export const mutations = {
  async registerContractInstance (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerContractInstance mutation: ', payload);
    state.contract.instance = () => payload
  },
  registerContractNetwork (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerContractNetwork mutation: ', payload);
    state.contract.network = payload
  },
  registerContractAddress (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerContractAddress mutation: ', payload);
    state.contract.address = payload
    state.contract.url = ETHERSCAN_URL(state.contract.network, '/address/'+state.contract.address)
  },
  registerCreator (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerCreator mutation: ', payload);
    state.creator.address = payload
    state.creator.url = ETHERSCAN_URL(state.contract.network, '/address/'+payload)
  },
  registerUpdator (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerUpdator mutation: ', payload);
    state.updator.address = payload
    state.updator.url = ETHERSCAN_URL(state.contract.network, '/address/'+payload)
  },
  registerHash (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerHash mutation: ', payload);
    state.hash = payload
  },
  registerRemoteData (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerRemoteData mutation: ', payload);
    state.data.remote = payload
  },
  registerRead (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerRead mutation: ', payload);
    state.read = payload
  },
  registerLocalData (state, payload) {
    Notifier.log('SHARED_TABLE_STORE: registerLocalData mutation: ', payload);
    state.data.local = payload
  },
};

export const actions = {
  setLocalData({commit}, data) {
    Notifier.log('SHARED_TABLE_STORE: registerLocalData action', data)
    commit('registerLocalData', data);
  },
  async getContractInstance (context) {
    Notifier.log('SHARED_TABLE_STORE: getContractInstance action')
    let getContract = new Promise(function (resolve, reject) {
      let web3 = new Web3(window.web3.currentProvider)
      let sharedTableContractInstance = new web3.eth.Contract(SHARED_TABLE.abi, SHARED_TABLE.address);

      resolve(sharedTableContractInstance)
    });


    try {
      let contract = await getContract;
      let address = await contract.address;

      context.commit('registerContractNetwork', SHARED_TABLE.network);
      context.commit('registerContractAddress', address);
      context.commit('registerContractInstance', contract);
      context.dispatch('listen');
    } catch(e) {
      context.dispatch('listen');
    }
  },
  listen(context) {
    Notifier.log('SHARED_TABLE_STORE: listen action')
    context.state.contract.instance().events.hashUpdated().on('data', function(event) {
      this.dispatch('readContract')
    }.bind(context));
  },
  async readContract ({state, commit}) {
    Notifier.log('SHARED_TABLE_STORE: readContract action')

    commit('registerCreator', await state.contract.instance().methods.created_by().call());
    commit('registerUpdator', await state.contract.instance().methods.updated_by().call());

    let hash = await state.contract.instance().methods.currentHash().call();
    commit('registerHash', hash);
    commit('registerRead', false);
    if (hash) {
      Notifier.log('shared_table STORE: reading hash ', hash);
      let ipfs = await IPFS_MANAGER.read(hash);
      ipfs = JSON.parse(ipfs);
      commit('registerRemoteData', ipfs.data);
      commit('registerRead', true);
    }
  },
  async pull ({state, commit}) {
    let local = null;
    if (state.data.remote) {
      local = state.data.remote.slice(0);
    }
    commit('registerLocalData', local);
  }
}
