import SHARED_TABLE from './shared_table'
import SHARED_DAB from './dab'

let sm = {};
sm.shared_table = SHARED_TABLE.smartcontract;
sm.dab = SHARED_DAB.smartcontract;

export default sm;
