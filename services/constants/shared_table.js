const NETWORK = 3;
const ADDRESS = '0xa3dd46b78b2c80cccc1f7061d9e975f611718c9a';
const ABI = [
  {
    "constant": false,
    "inputs": [
      {
        "name": "_currentHash",
        "type": "string"
      },
      {
        "name": "_newHash",
        "type": "string"
      }
    ],
    "name": "updateHash",
    "outputs": [
      {
        "name": "",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [],
    "name": "hashUpdated",
    "type": "event"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "created_by",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "currentHash",
    "outputs": [
      {
        "name": "",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "updated_by",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  }
];

const SMARTCONTRACT = `
pragma solidity ^0.5.1;
contract Shared_Table {

    address public created_by;
    address public updated_by;
    string public currentHash;

    event hashUpdated(
    );

    constructor () public {
        created_by = msg.sender;
    }

    function updateHash(string memory _currentHash, string memory _newHash) public returns (bool) {
      require(keccak256(abi.encodePacked(_currentHash)) == keccak256(abi.encodePacked(currentHash)), "Your current version is not up-to-date, concurrency problem ?");

      currentHash = _newHash;
      updated_by = msg.sender;
      emit hashUpdated();
      return true;
    }
}
`;

export default {
  network: NETWORK,
  address: ADDRESS,
  abi: ABI,
  smartcontract: SMARTCONTRACT
}
