import dab from '~/models/dab.js'

export default (context, inject) => {

  const model = {
    dab: dab(context)
  }

  inject('model', model)
}
